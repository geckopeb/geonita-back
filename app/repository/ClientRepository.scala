package repository

import contexts.DBExecutionContext

import javax.inject.{Inject, Singleton}
import db.ClientDB
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.Future

@Singleton
class ClientRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit DBExecutionContext: DBExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class ClientTable(tag: Tag) extends Table[ClientDB](tag, "client") {
    def id = column[String]("id", O.PrimaryKey)
    def name = column[String]("name")
    def description = column[Option[String]]("description")

    def * = (id, name, description) <> ((ClientDB.apply _).tupled, ClientDB.unapply)
  }

  val clients = TableQuery[ClientTable]

  def all(): Future[Seq[ClientDB]] = {
    db.run {
      clients.result
    }
  }

  def getById(id: String): Future[Option[ClientDB]] = {
    db.run {
      clients.filter(_.id === id).result.headOption
    }
  }
}
