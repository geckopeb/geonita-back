package repository

import contexts.DBExecutionContext
import javax.inject.{Inject, Singleton}
import db.WellStatusRelationshipDB
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.Future

@Singleton
class WellStatusRelationshipRepository @Inject() (
  dbConfigProvider: DatabaseConfigProvider,
  val wellRepository: WellRepository,
  val wellStatusRepository: WellStatusRepository
)(implicit DBExecutionContext: DBExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class WellStatusRelationshipTable(tag: Tag) extends Table[WellStatusRelationshipDB](tag, "well_status_relationship"){
    def wellId = column[String]("well_id")
    def wellStatusId = column[String]("well_status_id")

    def * = (wellId, wellStatusId) <> ((WellStatusRelationshipDB.apply _).tupled, WellStatusRelationshipDB.unapply)

    val wellIdKey = foreignKey("well_status_relationship_well_id", wellId, wellRepository.wells)(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
    val wellStatusIdKey = foreignKey("well_status_relationship_well_status_id", wellStatusId, wellStatusRepository.wellStatus)(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
  }

  val wellStatusRelationship = TableQuery[WellStatusRelationshipTable]

  def all(): Future[Seq[WellStatusRelationshipDB]] = {
    db.run {
      wellStatusRelationship.result
    }
  }
}
