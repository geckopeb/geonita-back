package repository

import contexts.DBExecutionContext
import javax.inject.{Inject, Singleton}
import db.WellStatusDB
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.Future

@Singleton
class WellStatusRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit DBExecutionContext: DBExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class WellStatusTable(tag: Tag) extends Table[WellStatusDB](tag, "well_status") {
    def id = column[String]("id", O.PrimaryKey)
    def name = column[String]("name")
    def description = column[Option[String]]("description")
    def color = column[String]("color")

    def * = (id, name, description, color) <> ((WellStatusDB.apply _).tupled, WellStatusDB.unapply)
  }

  def wellStatus = TableQuery[WellStatusTable]

  def all(): Future[Seq[WellStatusDB]] = {
    db.run {
      wellStatus.result
    }
  }
}
