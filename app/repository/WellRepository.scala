package repository

import contexts.DBExecutionContext
import javax.inject.{Inject, Provider, Singleton}
import db.WellDB
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.Future

@Singleton
class WellRepository @Inject()  (
  dbConfigProvider: DatabaseConfigProvider,
  val siteRepository: SiteRepository,
  val wellStatusRelationshipRepository: Provider[WellStatusRelationshipRepository]
)(implicit DBExecutionContext: DBExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class WellTable(tag: Tag) extends Table[WellDB](tag, "well") {
    def id = column[String]("id", O.PrimaryKey)
    def name = column[String]("name")
    def siteId = column[String]("site_id")
    def elevation = column[Double]("elevation")
    def description = column[Option[String]]("description")

    def * = (id, name, siteId, elevation, description) <> ((WellDB.apply _).tupled, WellDB.unapply)

    val clientIdKey = foreignKey("well_site_id", siteId, siteRepository.sites)(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
  }

  val wells = TableQuery[WellTable]

  def all(): Future[Seq[WellDB]] = {
    db.run {
      wells.result
    }
  }

  def getFromSiteId(siteId: String): Future[Seq[WellDB]] = {
    db.run {
      wells.filter(_.siteId === siteId).result
    }
  }

  def getFromSiteIdWithStatus(siteId: String): Future[Seq[(WellDB, Option[String])]] = {
    val query = for {
      (w, ws) <- wells.joinLeft(wellStatusRelationshipRepository.get().wellStatusRelationship).on(_.id === _.wellId) if w.siteId === siteId
    } yield (w, ws.map(_.wellStatusId))

    db.run(query.result)
  }
}
