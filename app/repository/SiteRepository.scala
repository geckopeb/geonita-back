package repository

import contexts.DBExecutionContext
import javax.inject.{Inject, Singleton}
import db.SiteDB
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.Future

@Singleton
class SiteRepository @Inject() (dbConfigProvider: DatabaseConfigProvider, val clientRepository: ClientRepository)(implicit DBExecutionContext: DBExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class SiteTable(tag: Tag) extends Table[SiteDB](tag, "site") {
    def id = column[String]("id", O.PrimaryKey)
    def name = column[String]("name")
    def clientId = column[String]("client_id")
    def description = column[Option[String]]("description")

    def * = (id, name, clientId, description) <> ((SiteDB.apply _).tupled, SiteDB.unapply)

    val clientIdKey = foreignKey("site_client_id", clientId, clientRepository.clients)(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
  }

  val sites = TableQuery[SiteTable]

  def all(): Future[Seq[SiteDB]] = {
    db.run {
      sites.result
    }
  }

  def getById(id: String): Future[Option[SiteDB]] = {
    db.run {
      sites.filter(_.id === id).result.headOption
    }
  }

  def getByClientId(clientId: String): Future[Seq[SiteDB]] = {
    db.run {
      sites.filter(_.clientId === clientId).result
    }
  }
}
