package db

case class SiteDB(
  id: String,
  name: String,
  clientId: String,
  description: Option[String]
) extends BaseDB