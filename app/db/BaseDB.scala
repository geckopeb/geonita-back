package db

trait BaseDB {
  def id: String
  def name: String
  def description: Option[String]
}
