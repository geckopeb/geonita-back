package db

case class WellStatusDB(id: String, name: String, description: Option[String], color: String) extends BaseDB