package db

case class ClientDB(id: String, name: String, description: Option[String]) extends BaseDB