package db

case class WellDB(id: String, name: String, siteId: String, elevation: Double, description: Option[String]) extends BaseDB