package model

import com.fasterxml.jackson.annotation.JsonUnwrapped
import db.{WellDB, WellStatusDB}

case class Well(@JsonUnwrapped well: WellDB, statuses: Seq[String])

case class SiteWells(wells: Seq[Well], references: Map[String, WellStatusDB])