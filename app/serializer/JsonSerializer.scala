package serializer

import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper, PropertyNamingStrategy}
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import javax.inject.Singleton

@Singleton
class JsonSerializer
  extends ObjectMapper with ScalaObjectMapper {

  this.registerModule(new JavaTimeModule).registerModule(DefaultScalaModule)
  this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
  this.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES,true)
  this.setSerializationInclusion(Include.NON_ABSENT)
  this.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)

}
