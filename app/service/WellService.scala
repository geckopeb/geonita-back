package service

import model.{Well, SiteWells}
import javax.inject.{Inject, Singleton}
import db.{WellDB, WellStatusDB}
import repository.{WellRepository, WellStatusRepository}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class WellService @Inject() (
  wellRepository: WellRepository,
  wellStatusRepository: WellStatusRepository
)(implicit ec: ExecutionContext) {

  private def buildReferences(refs: Seq[WellStatusDB], availableStatuses: Set[String]): Map[String, WellStatusDB] = {
     refs.filter(r => availableStatuses.contains(r.id)).map(ws => (ws.id, ws)).toMap
  }

  def build(wells: Seq[(WellDB, Option[String])], references: Seq[WellStatusDB]): Option[SiteWells] = {
    if(wells.nonEmpty) {
      val w = wells.groupBy(_._1).map{case(well, statuses) => Well(well, statuses.flatMap(_._2))}

      Some(SiteWells(w.toSeq, buildReferences(references, w.flatMap(_.statuses).toSet)))
    } else {
      None
    }
  }

  def getWellsFromSite(siteId: String): Future[Option[SiteWells]] = {
    for {
      wells <- wellRepository.getFromSiteIdWithStatus(siteId)
      references <- wellStatusRepository.all()
    } yield build(wells, references)
  }
}
