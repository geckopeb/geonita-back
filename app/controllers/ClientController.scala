package controllers

import javax.inject.Inject
import play.api.mvc._
import repository.ClientRepository
import serializer.JsonSerializer

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


class ClientController @Inject() (
  val controllerComponents: ControllerComponents,
  clientRepository: ClientRepository,
  serializer: JsonSerializer
)(implicit executionContext: ExecutionContext) extends BaseController {

  def getClients: Action[AnyContent] = Action.async {
    clientRepository.all().map {
      clients => Ok(serializer.writeValueAsString(clients))
    }
  }

  def getClient(id: String): Action[AnyContent] = Action.async {
    clientRepository.getById(id).map(
      _ match {
        case Some(c) => Ok(serializer.writeValueAsString(c))
        case None => NotFound
        case _ => InternalServerError
      }
    )
  }
}
