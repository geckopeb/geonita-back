package controllers

import javax.inject.Inject
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import repository.SiteRepository
import serializer.JsonSerializer

import scala.concurrent.ExecutionContext


class SiteController @Inject() (
  val controllerComponents: ControllerComponents,
  siteRepository: SiteRepository,
  serializer: JsonSerializer
)(implicit executionContext: ExecutionContext) extends BaseController {
  def getSites: Action[AnyContent] = Action.async {
    siteRepository.all().map {
      sites => Ok(serializer.writeValueAsString(sites))
    }
  }

  def getSite(id: String): Action[AnyContent] = Action.async {
    siteRepository.getById(id).map { site =>
      site match {
        case Some(s) => Ok(serializer.writeValueAsString(s))
        case None => NotFound
      }
    }
  }

  def getSitesByClientId(clientId: String): Action[AnyContent] = Action.async {
    siteRepository.getByClientId(clientId).map {
      sites => Ok(serializer.writeValueAsString(sites))
    }
  }
}
