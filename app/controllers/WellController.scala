package controllers

import javax.inject.Inject
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import serializer.JsonSerializer
import service.WellService

import scala.concurrent.ExecutionContext

class WellController @Inject() (
  val controllerComponents: ControllerComponents,
  wellService: WellService,
  serializer: JsonSerializer
)(implicit executionContext: ExecutionContext) extends BaseController {
  def getWellsBySiteId(siteId: String): Action[AnyContent] = Action.async {
    wellService.getWellsFromSite(siteId).map { res =>
      res match {
        case Some(wellSiteDTO) => Ok(serializer.writeValueAsString(wellSiteDTO))
        case None => NotFound
      }
    }
  }
}
