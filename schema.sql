/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.30-0ubuntu0.18.04.1 : Database - geonita
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`geonita` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `geonita`;

/*Table structure for table `client` */

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `site` */

DROP TABLE IF EXISTS `site`;

CREATE TABLE `site` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `client_id` char(36) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `site_client_id` (`client_id`),
  CONSTRAINT `site_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `well` */

DROP TABLE IF EXISTS `well`;

CREATE TABLE `well` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `site_id` char(36) NOT NULL,
  `elevation` float NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `well_site_id` (`site_id`),
  CONSTRAINT `well_site_id` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `well_status` */

DROP TABLE IF EXISTS `well_status`;

CREATE TABLE `well_status` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `color` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `well_status_relationship` */

DROP TABLE IF EXISTS `well_status_relationship`;

CREATE TABLE `well_status_relationship` (
  `well_id` char(36) NOT NULL,
  `well_status_id` char(36) NOT NULL,
  PRIMARY KEY (`well_id`,`well_status_id`),
  KEY `well_status_relationship_well_status_id` (`well_status_id`),
  CONSTRAINT `well_status_relationship_well_id` FOREIGN KEY (`well_id`) REFERENCES `well` (`id`),
  CONSTRAINT `well_status_relationship_well_status_id` FOREIGN KEY (`well_status_id`) REFERENCES `well_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
